$(function () {
  $("#load-imgs")
    .imagesLoaded()
    .always(function (e) {
      if (e.isComplete) {
        TweenMax.to("#loading", 0.8, {
          autoAlpha: 0,
          display: "none",
          ease: Linear.easeNone,
        });

        init.play();
      }
    });


  var init = new TimelineMax();
  init
    .from(".event-kv-rainbow", 0.8, {
      y: 100,
      ease: Quart.easeOut,
      autoAlpha: 0,
    })
    .from(
      ".event-kv-prod",
      1,
      {
        y: -20,
        scale: 1.2,
        ease: Back.easeOut,
        autoAlpha: 0,
      },"-=.8")
    .from(
      ".event-kv-title",
      1,
      {
        scale: 0.2,
        ease: Back.easeOut,
        autoAlpha: 0,
      },"-=.3")
      .from(
        ".event-kv-txt,.event-kv-txt1",
        1,
        {
          x: -20,
          ease: Back.easeOut,
          autoAlpha: 0,
        },"-=.5")
      .from(
        ".event-kv-txt2,.event-kv-iphone",
        1,
        {
          x: -50,
          ease: Back.easeOut,
          autoAlpha: 0,
        },"-=.8")
      .from(
        ".event-kv-btn",
        1,
        {
          x: -50,
          ease: Back.easeOut,
          autoAlpha: 0,
        },"-=.8");

    

  init.pause();


  $('.mobile-btn-menu').click(function(){
    $(this).toggleClass('active');

    if($(this).hasClass('active')){
      TweenMax.to(".mobile-menu", 0.8, {
        display: 'block',
        ease: Quart.easeOut,
        autoAlpha: 1,
      });
      $('html').addClass('lock-body');
    }else{
      TweenMax.to(".mobile-menu", 0.8, {
        display: 'none',
        ease: Quart.easeOut,
        autoAlpha: 0,
      });
      $('html').removeClass('lock-body');
    }
  });

 
});
