$(function () {
  $("#load-imgs")
    .imagesLoaded()
    .always(function (e) {
      if (e.isComplete) {
        TweenMax.to("#loading", 0.8, {
          autoAlpha: 0,
          display: "none",
          ease: Linear.easeNone,
        });

        init.play();
      }
    });


  var init = new TimelineMax();
  init
    .from(".event-kv-rainbow", 0.8, {
      y: 100,
      ease: Quart.easeOut,
      autoAlpha: 0,
    })
    .from(
      ".event-kv-prod",
      1,
      {
        y: -20,
        scale: 1.2,
        ease: Back.easeOut,
        autoAlpha: 0,
      },"-=.8")
    .from(
      ".event-kv-title",
      1,
      {
        scale: 0.2,
        ease: Back.easeOut,
        autoAlpha: 0,
      },"-=.3")
      .from(
        ".event-kv-txt,.event-kv-txt1",
        1,
        {
          x: -20,
          ease: Back.easeOut,
          autoAlpha: 0,
        },"-=.5")
      .from(
        ".event-kv-txt2,.event-kv-iphone",
        1,
        {
          x: -50,
          ease: Back.easeOut,
          autoAlpha: 0,
        },"-=.8")
      .from(
        ".event-kv-btn",
        1,
        {
          x: -50,
          ease: Back.easeOut,
          autoAlpha: 0,
        },"-=.8");

    

  init.pause();


  $('.mobile-btn-menu').click(function(){
    $(this).toggleClass('active');

    if($(this).hasClass('active')){
      TweenMax.to(".mobile-menu", 0.8, {
        display: 'block',
        ease: Quart.easeOut,
        autoAlpha: 1,
      });
      $('html').addClass('lock-body');
    }else{
      TweenMax.to(".mobile-menu", 0.8, {
        display: 'none',
        ease: Quart.easeOut,
        autoAlpha: 0,
      });
      $('html').removeClass('lock-body');
    }
  });

  $('.back-top a').click(function(e){
    e.preventDefault();
    $("body,html").animate({
      scrollTop: 0,
    });
  });

  $('#event-kv .scroll').click(function(e){
    e.preventDefault();
    $("body,html").animate({
      scrollTop: $('#event-invoice').offset().top - $('#header').height(),
    });
  });

  $('a.goto').click(function(e){
    e.preventDefault();
    
    let got = $(this).attr('href');
    $("body,html").animate({
      scrollTop: $(got).offset().top - $('#header').height(),
    });

    if($(window).width() < 768){
      $('.mobile-btn-menu').click();
    }
  });

  $('a.scrollTo').click(function(e){
    e.preventDefault();
    
    let got = $(this).attr('href');
    $("body,html").animate({
      scrollTop: $(got).offset().top - $('#header').height(),
    });
  });

  
  $('.btn-add').click(function(e){
    e.preventDefault();

    let html = `<div class="row form-group">
    <div class="col">
      <select name="" id="" class="custom-select-style">
        <option value="0">請選擇購買通路</option>
        <option value="1">7-11</option>
        <option value="2">全家</option>
        <option value="3">萊爾富</option>
        <option value="4">OK超商</option>
        <option value="5">全聯</option>
        <option value="6">量販店</option>
        <option value="7">其他通路</option>
      </select>
    </div>
    <div class="col"><input type="text" class="custom-style" placeholder="輸入統一發票號碼(共十碼)"></div>
  </div>`;

  $('#new-invoice').append(html);
  });

  

  // // Instantiate new modal
  var modal = new Custombox.modal({
    content: {
      effect: "fadein",
      target: "#banner-lb",
    },
  });

  var setting = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: false,
    speed: 600,
    dots: true,
  }
  $('#banner-lb').show();
  $('#banner-lb').hide();
  $("#banner-slide").slick(setting);

  $('.btn-submit').click(function(e) {
    e.preventDefault();
    
    $("#banner-slide").slick('refresh');
    modal.open();
    
    
  })
  

});
